# nvim-init
This is the NeoVim configuration file that I use on almost all my setups.

# vim-plug
In order to install plugins I'm using vim-plug. To install it run this commands in your terminal

```bash
$ curl -fLo ~/.local/share/nvim/site/autoload/plug.vim --create-dirs \
    https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
```

# ctags
I use the Gutentags and Gutentags_plus plugins because of the idea of project based on .git folder and because of having the possibility to define an external directory to store the tags
You have to have ctags-like app and also gtags_cscope. For ctags I've switched to universal-ctags.
## universal-ctags
To install universal-ctags in ubuntu you have to follow these instructions

```bash
$ sudo apt install \
    build-essentials \
    pkg-config autoconf automake \
    python3-docutils \
    libseccomp-dev \
    libjansson-dev \
    libyaml-dev \
    libxml2-dev

$ git clone https://github.com/universal-ctags/ctags.git
$ cd ctags
$ ./autogen.sh
$ ./configure
$ make
$ sudo make install
```

## gtags_cscope
To install gtags_cscope in ubuntu 18.04 you have to follow these instructions

```bash
$ sudo apt-get update
$ sudo apt-get install global
```

# fzf
In order to use the fzf plugin you have to have the fzf binaries already installed in your system.
The easiest what to install it is the following:

```bash
git clone --depth 1 https://github.com/junegunn/fzf.git ~/.fzf
~/.fzf/install
```

# CoC vim
After several trials I believe that nowadays the best completion engine is coc.vim
To install it you have to follow this instructions:
1. Install nodejs
```bash
$ curl -sL install-node.now.sh/lts | sudo bash 
```
2. Add coc.vim to the init.vim file and restart nvim. To see if its working run :checkhealth command
3. Exit nvim and install clangd (for C/C++ autocompletion)
```bash
$ sudo apt-get install clangd-9
$ sudo update-alternatives --install /usr/bin/clangd clangd /usr/bin/clangd-9 100
```
4. Install coc-clangd plugin in nvim by running :CocInstall coc-clangd
5. Install coc-snippets plugin in nvim by running :CocInstall coc-snippets
6. In order to use clangd you have to provide the compilation instructions. see clangd documentation for more information but if you are using an standard make toolchain you have to install bear.
```bash
$ sudo apt-get install bear
```
7. Install coc-spell-checker plugin by running :CocInstall coc-spell-checker

# Key-mappings
Key | Function
----|---------
\<SPACEBAR\>| Leader key
\<F2\>| Toggle directory tree panel on/off
\<F3\>| gtags search for symbol definition
\<F4\>| gtags search for symbol usage
\<F5\>| gtags search for caller functions
\<F8\>| Toggle tagbar panel on/off
\<leader\> c \<leader\>| Comment / uncomment selected lines
zz | Save file if there are changes (:update)


